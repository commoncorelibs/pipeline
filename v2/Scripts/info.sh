#!/usr/bin/env bash
# Copyright (c) 2022 Jay Jeckel
# Licensed under the MIT license: https://opensource.org/licenses/MIT
# Permission is granted to use, copy, modify, and distribute the work.
# Full license information available in the project LICENSE file.

set -o errexit
set -o nounset
set -o pipefail
#set -o xtrace

EchoSection "USEFUL VARIABLES"

echo "pipeline_repo: ${pipeline_repo:-null}"
echo "pipeline_path: ${pipeline_path:-null}"
EchoSectionAnd

echo "company: ${company:-null}"
echo "project: ${project:-null}"
echo "version: ${version:-null}"
echo "title:   ${title:-null}"
EchoSectionAnd

echo "CI_REPOSITORY_URL:     ${CI_REPOSITORY_URL:-}"
echo "CI_API_V4_URL:         ${CI_API_V4_URL:-}"
echo "CI_JOB_TOKEN:          ${CI_JOB_TOKEN:-}"
EchoSectionAnd

echo "CI_PROJECT_NAMESPACE:  ${CI_PROJECT_NAMESPACE:-}"
echo "CI_PROJECT_TITLE:      ${CI_PROJECT_TITLE:-}"
echo "CI_PROJECT_NAME:       ${CI_PROJECT_NAME:-}"
echo "CI_PROJECT_ID:         ${CI_PROJECT_ID:-}"
echo "CI_PROJECT_PATH:       ${CI_PROJECT_PATH:-}"
echo "CI_PROJECT_DIR:        ${CI_PROJECT_DIR:-}"
echo "CI_PROJECT_URL:        ${CI_PROJECT_URL:-}"
echo "CI_PROJECT_TAG:        ${CI_PROJECT_TAG:-}"
EchoSectionAnd

echo "CI_COMMIT_REF_NAME:    ${CI_COMMIT_REF_NAME:-}"
echo "CI_COMMIT_BRANCH:      ${CI_COMMIT_BRANCH:-}"
echo "CI_COMMIT_SHA:         ${CI_COMMIT_SHA:-}"
echo "CI_COMMIT_SHORT_SHA:   ${CI_COMMIT_SHORT_SHA:-}"
echo "CI_COMMIT_TITLE:       ${CI_COMMIT_TITLE:-}"
echo "CI_COMMIT_DESCRIPTION: ${CI_COMMIT_DESCRIPTION:-}"
echo "CI_COMMIT_MESSAGE:     ${CI_COMMIT_MESSAGE:-}"
echo "CI_COMMIT_TAG:         ${CI_COMMIT_TAG:-}"
EchoSectionAnd

echo "CI_PAGES_DOMAIN:       ${CI_PAGES_DOMAIN:-}"
echo "CI_PAGES_URL:          ${CI_PAGES_URL:-}"
EchoSectionAnd

echo "BASE_PATH:  ${BASE_PATH:-null}"
echo "PAGES_PATH: ${PAGES_PATH:-null}"
echo "STORE_PATH: ${STORE_PATH:-null}"
echo "__dirpath:  ${__dirpath:-null}"
echo "__file:     ${__file:-null}"
echo "__filename: ${__filename:-null}"
echo "__fileext:  ${__fileext:-null}"
echo "__filepath: ${__filepath:-null}"
EchoSectionAnd

EchoDirectories
EchoFiles
EchoSectionEnd
