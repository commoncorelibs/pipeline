#!/usr/bin/env bash
# Copyright (c) 2022 Jay Jeckel
# Licensed under the MIT license: https://opensource.org/licenses/MIT
# Permission is granted to use, copy, modify, and distribute the work.
# Full license information available in the project LICENSE file.

set -o errexit
set -o nounset
set -o pipefail
#set -o xtrace

function EchoThickLine { echo "======================================================================"; }
function EchoThinLine { echo "----------------------------------------------------------------------"; }
function EchoSectionAnd { EchoThinLine; }
function EchoSectionEnd { EchoThickLine; }
function EchoSection
{
    if [ -z "${1:-}" ]; then echo "ERROR: Function title argument is empty or not set."; exit 1; fi;
    EchoThickLine; echo "${1:-}"; EchoThinLine;
}

function EchoKeyValue
{
    if [ -z "${1:-}" ]; then echo "ERROR: Function key argument is empty or not set."; exit 1; fi;
    if [ -z "${2:-}" ]; then echo "ERROR: Function value argument is empty or not set."; exit 1; fi;
    echo "${1:-}: ${2:-}";
}

#function Test { local path="$(pwd)"; cd ../..; ls --directory */; cd "${path}"; }
function EchoDirectories { ls --directory */; }

function EchoFiles { ls --file-type --sort=extension | grep -v /; }

function RequireVariable { if [ -z "${1:-}" ]; then echo "ERROR: Variable '${2}' is empty or not set."; exit 1; fi; }

function EnsurePath
{
    if [ -z "${1:-}" ]; then echo "ERROR: Function path argument is empty or not set."; exit 1; fi;
    mkdir --parents "${1:-}"; echo "${1:-}";
}

function InstallAptGet
{
    if [ -z "${1:-}" ]; then echo "ERROR: Function package name argument is empty or not set."; exit 1; fi;

    EchoSection "INSTALLING ${1:-}"
    if [ -x "$(command -v "${1:-}")" ]; then echo "Requested package '${1:-}' is already installed.";
    else apt-get update -y -qq && apt-get install -y -qq --allow-unauthenticated "${1:-}"; fi;
    EchoSectionEnd
}

function InstallDotNetTool
{
    if [ -z "${1:-}" ]; then echo "ERROR: Function package name argument is empty or not set."; exit 1; fi;
    if [ -z "${2:-}" ]; then echo "ERROR: Function package tool path argument is empty or not set."; exit 1; fi;
    
    EchoSection "INSTALLING ${1:-}"
    dotnet tool install "${1:-}" --tool-path "${2:-}"
    EchoSectionEnd
}

function LoadFileContents
{
    if [ -z "${1:-}" ]; then echo "ERROR: Function path argument is empty or not set."; exit 1; fi;

    echo "$(cat "${1:-}" 2> /dev/null || echo "${2:-}")";
}

#function LoadVersion { version="$(cat ./version.txt)"; version="${version:-0.0.0}"; }
function LoadVersion
{
    if [ -z "${1:-}" ]; then echo "ERROR: Function path argument is empty or not set."; exit 1; fi;

    echo "$(cat "${1:-}" 2> /dev/null || echo "0.0.0")";
}

function DevBumpVersion
{
    if [ -z "${1:-}" ]; then echo "ERROR: Function path argument is empty or not set."; exit 1; fi;

    local array=($(echo "${1:-0.0.0}" | tr '.-' ' '))
    if [ ${#array[@]} -eq 3 ]; then echo "${array[0]}.${array[1]}.$((${array[2]}+1))-pre.0";
    else echo "${array[0]}.${array[1]}.${array[2]}-${array[3]}.$((${array[4]}+1))"; fi;
}

function InstallLibGdiPlus
{
    echo "======================================================================"
    echo "INSTALLING libgdiplus"
    echo "----------------------------------------------------------------------"
    apt-get update -y -qq && apt-get install -y -qq --allow-unauthenticated libc6-dev libgdiplus libx11-dev
    rm -rf /var/lib/apt/lists/*
    echo "======================================================================"
}
