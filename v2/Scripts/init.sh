#!/usr/bin/env bash
# Copyright (c) 2022 Jay Jeckel
# Licensed under the MIT license: https://opensource.org/licenses/MIT
# Permission is granted to use, copy, modify, and distribute the work.
# Full license information available in the project LICENSE file.

set -o errexit
set -o nounset
set -o pipefail
#set -o xtrace

#__dirpath="$(cd "$(dirname "${__filepath}")" && pwd)"
#__file="$(basename "${BASH_SOURCE[0]}")"
__dirpath="${BASH_SOURCE[0]%/*}"
__filepath="${BASH_SOURCE[0]}"
__file="${BASH_SOURCE[0]##*/}"
__filename="$(basename "${BASH_SOURCE[0]}" .sh)"
__fileext="${BASH_SOURCE[0]##*.}"

echo "======================================================================"
echo "VERIFY BASH INSTALLED"
echo "----------------------------------------------------------------------"
bash --version
echo "======================================================================"
echo "VERIFY EXTERNAL FUNCTIONS"
echo "----------------------------------------------------------------------"
source "${__dirpath}/funcs.sh"
echo "======================================================================"
echo "VERIFY REQUIRED VARIABLES"
echo "----------------------------------------------------------------------"
echo "Verify Variable: company"
RequireVariable "${company:-}" "company"
echo "Final Value: ${company}"
echo "----------------------------------------------------------------------"
echo "Verify Variable: project"
RequireVariable "${project:-}" "project"
echo "Final Value: ${project}"
echo "----------------------------------------------------------------------"
echo "Verify Variable: pipeline_path"
RequireVariable "${pipeline_path:-}" "pipeline_path"
echo "Final Value: ${pipeline_path}"
echo "======================================================================"


EchoSection "INITIALIZE PATHS"

BASE_PATH="$(pwd)";
echo "BASE_PATH:  ${BASE_PATH}"

PAGES_PATH=$(EnsurePath "${BASE_PATH}/public");
echo "PAGES_PATH: ${PAGES_PATH}"

STORE_PATH=$(EnsurePath "${BASE_PATH}/store");
echo "STORE_PATH: ${STORE_PATH}"
EchoSectionEnd

EchoSection "INITIALIZE VERSION VARIABLE"

version="$(LoadVersion "${BASE_PATH}/version.txt")"
echo "Initial Value: ${version}"
if [ -z "${CI_COMMIT_TAG:-}" ]; then
    echo "Tagless Commit Detected: Bumping to temp dev version."
    version="$(DevBumpVersion "${version}")"
fi;
echo "Final Value:   ${version}"
EchoSectionEnd


EchoSection "INITIALIZE TITLE VARIABLE"

echo "Initial Value: ${title:-null}"
title="${company}.${project}.${version}"
echo "Final Value:   ${title}"
EchoSectionEnd


EchoSection "INITIALIZE RELEASE NOTES"

echo "Initial Value: ${release_notes:-null}"
release_notes="$(LoadFileContents "${BASE_PATH}/RELEASE_NOTES.md")"
echo "Final Value:"
echo "${release_notes:-null}"
EchoSectionEnd

newline=$'\n'

#public_path="${PAGES_PATH}"
#product_path="${public_path}/product"
#mkdir --parents "${product_path}"
#reports_path="${public_path}/reports"
#mkdir --parents "${reports_path}"
#reports_source_path="${reports_path}/_source"
#mkdir --parents "${reports_source_path}"
#stats_path="${public_path}/stats"
#mkdir --parents "${stats_path}"
