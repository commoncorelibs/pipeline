#!/usr/bin/env bash
# Copyright (c) 2022 Jay Jeckel
# Licensed under the MIT license: https://opensource.org/licenses/MIT
# Permission is granted to use, copy, modify, and distribute the work.
# Full license information available in the project LICENSE file.

set -o errexit
set -o nounset
set -o pipefail
#set -o xtrace

function RunTest
{
    InstallDotNetTool "coverlet.console" "${BASE_PATH}/tools"
    
    EchoSection "RUNNING dotnet test"
    dotnet test "${project}.Tests/${project}.Tests.csproj" -c Release
    EchoSectionEnd

    local path=$(EnsurePath "${STORE_PATH}/tests")
    
    EchoSection "RUNNING coverlet.console"
    "${BASE_PATH}"/tools/coverlet "${project}.Tests/bin/Release/net5.0/${project}.Tests.dll" --target "dotnet" \
        --targetargs "test ${project}.Tests/${project}.Tests.csproj --no-build -c Release" \
        --output "${path}/" -f json -f opencover | tee "${path}/coverage.txt"
    EchoSectionEnd
}
