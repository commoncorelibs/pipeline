# Pipeline Scripts

Provides script files for automating various stages of the CICD pipeline.

ATTENTION: The `init.sh` script must be run before using any of the other scripts. Best practice is to run it in the `before_script` attribute of the `default` section.

ATTENTION: Using the scripts in a job requires cloning the pipeline repo, so those jobs must use an image that includes `git`.

## Variables

The scripts require three variables to be set. The `init.sh` script will fail with exit code 1 if any of three variables is empty or not set.

The first two required variables are `company` and `project`. Best practice is to set these variables in the `variables` section along with the `pipeline_repo` variable that points to the [Pipeline](https://gitlab.com/commoncorelibs/pipeline) repo git url.

The `project` variable must match the primary project of the repo, not necessarily the name of the repo. For example, the **PipelineSandbox.Library** project is in the **PipelineSandbox** repo, so the variable should be set to the former, not the latter. Generally, the repo and project will share the same name and in these cases `project` should be set to `${CI_PROJECT_TITLE}`, which is the built-in global for the repo name.

```yaml
variables:
    pipeline_repo: https://gitlab.com/commoncorelibs/pipeline.git
    company: "VinlandSolutions"
    project: "PipelineSandbox.Library"
```

The third required variable is `pipeline_path`. This variable should be set to a temporary directory and exported in the `before_script` attribute of the `default` section.

```yaml
default:
    before_script:
        - export pipeline_path=$(mktemp -d)
```

## Initializing Scripts

Continuing in the `before_script` attribute of the `default` section, the next step is to clone the pipeline repo. This is done by passing `pipeline_repo` as the repo to clone and `pipeline_path` as the destination directory. We also then export a convenience variable, `scripts_path`, for use in calling scripts within this same yaml file. None of the pipeline scripts will try to use this convenience variable.

```yaml
        - git clone -q --depth 1 "${pipeline_repo}" "${pipeline_path}"
        - export scripts_path="${pipeline_path}/v1/Scripts"
```

Once the pipeline repo is cloned, the `init.sh` script can be called to initialize everything needed by the other scripts. However, before the script can be called, it has be given execution permission by the `chmod` command.

```yaml
        - chmod +x "${scripts_path}/init.sh"
        - . "${scripts_path}/init.sh"
```

After running the `init.sh` script, it is a good idea to then run the `info.sh` script. This script prints out a bunch of variables and there values, providing insight into the context around the job execution. Since the script doesn't actually __do__ anything, there is really no harm in giving oneself some extra information.

```yaml
        - chmod +x "${scripts_path}/info.sh"
        - . "${scripts_path}/info.sh"
```

Putting that all together, the gitlab ci yaml file should have `variables` and `default` sections that look similar to the following.

```yaml
variables:
    pipeline_repo: https://gitlab.com/commoncorelibs/pipeline.git
    company: "VinlandSolutions"
    project: "PipelineSandbox.Library"

default:
    before_script:
        - export pipeline_path=$(mktemp -d)
        - export scripts_path="${pipeline_path}/v1/Scripts"
        - git clone -q --depth 1 "${pipeline_repo}" "${pipeline_path}"
        - chmod +x "${scripts_path}/init.sh"
        - . "${scripts_path}/init.sh"
        - chmod +x "${scripts_path}/info.sh"
        - . "${scripts_path}/info.sh"
```

## Persisting Artifacts

TODO: Add text talking about the `artifacts` attribute and the `public` folder.

```yaml
default:
    artifacts:
        name: "artifacts"
        expire_in: 2 days
        paths: ['public']
```

## Using Scripts

The pipeline divided into four conceptual areas: test, build, pack, and deploy. These areas map to four stages that should be defined in the `stages` section.

```yaml
stages:
    - test
    - build
    - pack
    - deploy
```

Using the various scripts follows a consistent pattern; include the source of the related script and then call the desired method.

### Test and Build Stages

The `test` and `build` stages are as simple as one job each and two lines in each job's `script` attribute.

```yaml
test:
    extends: [.image_dotnet_5, .only_both, .when_success]
    stage: test
    script:
        - source "${scripts_path}/test.sh"
        - RunTest

build:
    extends: [.image_dotnet_5, .only_both, .when_success]
    stage: build
    script:
        - source "${scripts_path}/build.sh"
        - RunBuild
```

### Pack Stage

The `pack` stage is slightly more complex as it supports five or more jobs. Each packing job generates output that is stored under the `public` artifact directory that is shared between jobs. Similarly, some of the packing jobs, such as the badges and reports jobs, read information from the `public` artifact directory to control the generation of their output.

The packing of badges, docs, reports, and the nuget package are each two `script` lines, similar to the `text` and `build` jobs, sourcing the `pack.sh` file and calling the respective function.

```yaml
pack_badges:
    extends: [.image_python, .only_both, .when_success]
    stage: pack
    script:
        - source "${scripts_path}/pack.sh"
        - PackBadges

pack_docs:
    extends: [.image_docfx, .only_both, .when_success]
    stage: pack
    script:
        - source "${scripts_path}/pack.sh"
        - PackDocs

pack_reports:
    extends: [.image_dotnet_5, .only_both, .when_success]
    stage: pack
    script:
        - source "${scripts_path}/pack.sh"
        - PackReports

pack_nuget:
    extends: [.image_dotnet_5, .only_both, .when_success]
    stage: pack
    script:
        - source "${scripts_path}/pack.sh"
        - PackNuget
```

The `pack` stage gets slightly more complex when it comes to packing the product itself. In the case of the `PackZip` function, the platform and framework of the product must be set to generate the output zip. Therefor, the number of jobs is dependent on how many platforms and frameworks are supported and how the developer wishes to split them up.

In the simplest case where many platforms and frameworks are support, all can be packed in one job.

```yaml
pack_product:
    extends: [.image_dotnet_5, .only_both, .when_success]
    stage: pack
    script:
        - source "${scripts_path}/pack.sh"
        - PackZip "linux-x64" "netstandard2.0"
        - PackZip "linux-x64" "netstandard2.1"
        - PackZip "osx-x64" "netstandard2.0"
        - PackZip "osx-x64" "netstandard2.1"
        - PackZip "win-x64" "netstandard2.0"
        - PackZip "win-x64" "netstandard2.1"
```

Another option is to create a job for each platform, allowing them all to run in parallel.

```yaml
pack_linux:
    extends: [.image_dotnet_5, .only_both, .when_success]
    stage: pack
    script:
        - source "${scripts_path}/pack.sh"
        - PackZip "linux-x64" "netstandard2.0"
        - PackZip "linux-x64" "netstandard2.1"

pack_osx:
    extends: [.image_dotnet_5, .only_both, .when_success]
    stage: pack
    script:
        - source "${scripts_path}/pack.sh"
        - PackZip "osx-x64" "netstandard2.0"
        - PackZip "osx-x64" "netstandard2.1"

pack_win:
    extends: [.image_dotnet_5, .only_both, .when_success]
    stage: pack
    script:
        - source "${scripts_path}/pack.sh"
        - PackZip "win-x64" "netstandard2.0"
        - PackZip "win-x64" "netstandard2.1"
```

### Deploy Stage

The `deploy` stage supports three jobs: pages, nugets, and releases; each of which is a simple two-liner like `test` and `build`.

```yaml
pages:
    extends: [.image_dotnet_5, .only_both, .when_manual]
    stage: deploy
    script:
        - source "${scripts_path}/deploy.sh"
        - DeployPages

nugets:
    extends: [.image_dotnet_5, .only_tags, .when_manual]
    stage: deploy
    script:
        - source "${scripts_path}/deploy.sh"
        - DeployNuget

releases:
    extends: [.image_dotnet_5, .only_tags, .when_manual]
    stage: deploy
    script:
        - source "${scripts_path}/deploy.sh"
        - DeployRelease
```

## Example Gitlab Yaml

```yaml

include:
    - project: "commoncorelibs/pipeline"
      file: ["/Includes/templates.yml"]

variables:
    pipeline_repo: https://gitlab.com/commoncorelibs/pipeline.git
    company: "VinlandSolutions"
    project: "PipelineSandbox.Library"

default:
    image: bash:latest
    artifacts:
        name: "artifacts"
        expire_in: 2 days
        paths: ['public']
    before_script:
        - export pipeline_path=$(mktemp -d)
        - export scripts_path="${pipeline_path}/v1/Scripts"
        - git clone -q --depth 1 "${pipeline_repo}" "${pipeline_path}"
        - chmod +x "${scripts_path}/init.sh"
        - . "${scripts_path}/init.sh"
        - chmod +x "${scripts_path}/info.sh"
        - . "${scripts_path}/info.sh"

stages:
    - test
    - build
    - pack
    - deploy

test:
    extends: [.image_dotnet_5, .only_both, .when_success]
    stage: test
    script:
        - source "${scripts_path}/test.sh"
        - RunTest

build:
    extends: [.image_dotnet_5, .only_both, .when_success]
    stage: build
    script:
        - source "${scripts_path}/build.sh"
        - RunBuild

pack_badges:
    extends: [.image_python, .only_both, .when_success]
    stage: pack
    script:
        - source "${scripts_path}/pack.sh"
        - PackBadges

pack_docs:
    extends: [.image_docfx, .only_both, .when_success]
    stage: pack
    script:
        - source "${scripts_path}/pack.sh"
        - PackDocs

pack_reports:
    extends: [.image_dotnet_5, .only_both, .when_success]
    stage: pack
    script:
        - source "${scripts_path}/pack.sh"
        - PackReports

pack_nuget:
    extends: [.image_dotnet_5, .only_both, .when_success]
    stage: pack
    script:
        - source "${scripts_path}/pack.sh"
        - PackNuget

pack_linux:
    extends: [.image_dotnet_5, .only_both, .when_success]
    stage: pack
    script:
        - source "${scripts_path}/pack.sh"
        - PackZip "linux-x64" "netstandard2.0"
        - PackZip "linux-x64" "netstandard2.1"

pack_osx:
    extends: [.image_dotnet_5, .only_both, .when_success]
    stage: pack
    script:
        - source "${scripts_path}/pack.sh"
        - PackZip "osx-x64" "netstandard2.0"
        - PackZip "osx-x64" "netstandard2.1"

pack_win:
    extends: [.image_dotnet_5, .only_both, .when_success]
    stage: pack
    script:
        - source "${scripts_path}/pack.sh"
        - PackZip "win-x64" "netstandard2.0"
        - PackZip "win-x64" "netstandard2.1"

pages:
    extends: [.image_dotnet_5, .only_both, .when_manual]
    stage: deploy
    script:
        - source "${scripts_path}/deploy.sh"
        - DeployPages

nugets:
    extends: [.image_dotnet_5, .only_tags, .when_manual]
    stage: deploy
    script:
        - source "${scripts_path}/deploy.sh"
        - DeployNuget

releases:
    extends: [.image_dotnet_5, .only_tags, .when_manual]
    stage: deploy
    script:
        - source "${scripts_path}/deploy.sh"
        - DeployRelease

```
