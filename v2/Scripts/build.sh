#!/usr/bin/env bash
# Copyright (c) 2022 Jay Jeckel
# Licensed under the MIT license: https://opensource.org/licenses/MIT
# Permission is granted to use, copy, modify, and distribute the work.
# Full license information available in the project LICENSE file.

set -o errexit
set -o nounset
set -o pipefail
#set -o xtrace

function RunBuild
{
    EchoSection "RUNNING dotnet build"
    dotnet build ${project}/${project}.csproj -c Release /p:Version=${version}
    EchoSectionEnd
}
