#!/usr/bin/env bash
# Copyright (c) 2022 Jay Jeckel
# Licensed under the MIT license: https://opensource.org/licenses/MIT
# Permission is granted to use, copy, modify, and distribute the work.
# Full license information available in the project LICENSE file.

set -o errexit
set -o nounset
set -o pipefail
#set -o xtrace

source "${__dirpath}/pack_funcs.sh"

function PackNuget
{
    local product_path=$(EnsurePath "${STORE_PATH}/product")

    EchoSection "RUNNING dotnet pack"
    dotnet pack "${project}/${project}.csproj" --nologo \
        --configuration "Release" \
        --output "${product_path}" \
        /p:Version="${version}" \
        /p:PackageReleaseNotes=\""${release_notes}"\"
    EchoSectionEnd
}

function PackZip
{
    InstallAptGet "zip"
    
    local platform="${1}"
    local framework="${2:-}"
    local temp_path=$(mktemp -d)
    local zip_path=""
    local product_path=$(EnsurePath "${STORE_PATH}/product")

    if [ -z "${framework}" ]; then
        zip_path="${product_path}/${title}-${platform}.zip"
    else
        zip_path="${product_path}/${title}-${platform}-${framework}.zip"
    fi;

    EchoSection "GENERATING FILES"
    echo "platform:  ${platform}"
    echo "framework: ${framework:-null}"
    EchoSectionAnd
    if [ -z "${framework}" ]; then
        dotnet publish ${project}/${project}.csproj -r "${platform}" --self-contained false -c Release -o "${temp_path}" /p:Version=${version}
    else
        dotnet publish ${project}/${project}.csproj -r "${platform}" --framework "${framework}" --self-contained false -c Release -o "${temp_path}" /p:Version=${version}
    fi;
    EchoSectionEnd
    
    EchoSection "ZIPPING FILES"
    echo "zip_path: ${zip_path}"
    cd "${temp_path}"
    zip -r "${zip_path}" *
    cd "${BASE_PATH}"
    EchoSectionEnd
}

function PackDocs
{
    EchoSection "COPY BEFORE SOURCES"
    local before_path=$(EnsurePath "${STORE_PATH}/_docs/before")
    cp -r --verbose "${before_path}/." "${project}.Docs"
    EchoSectionEnd
    
    EchoSection "RUNNING DOCFX"
    docfx ${project}.Docs/docfx.json --force
    EchoSectionEnd
    
    EchoSection "PACK DOCS"
    cp -r --verbose "${project}.Docs/_site/." "${PAGES_PATH}"
    EchoSectionEnd
    
    EchoSection "COPY AFTER SOURCES"
    local after_path=$(EnsurePath "${STORE_PATH}/_docs/after")
    cp -r --verbose "${after_path}/." "${PAGES_PATH}"
    EchoSectionEnd
}

function PackReports
{
    PackReportsTestCoverage
    PackReportsLoc
}

function PackReportsTestCoverage
{
    InstallDotNetTool "dotnet-reportgenerator-globaltool" "${BASE_PATH}/tools"
    
    local path=$(EnsurePath "${STORE_PATH}/tests")
    
    EchoSection "RUNNING reportgenerator"
    "${BASE_PATH}"/tools/reportgenerator "-reports:${path}/coverage.opencover.xml" "-targetdir:${path}" \
    "-reportTypes:MarkdownSummary"
    EchoSectionAnd
    mv --verbose "${path}/Summary.md" "${path}/index.md"
    echo "- name: Summary" > "${path}/toc.yml"; echo "  href: index.md" >> "${path}/toc.yml";
    EchoSectionEnd
    
    EchoSection "BUILDING DOCS FILES"
    local reports_path=$(EnsurePath "${STORE_PATH}/_docs/before/reports")
    cat "${path}/index.md" > "${reports_path}/tests.md"
    EchoSectionAnd
    local tests_path=$(EnsurePath "${reports_path}/tests")
    cp -r --verbose "${path}/." "${tests_path}"
    EchoSectionEnd
}

function PackReportsLoc
{
    InstallAptGet "cloc"

    local path=$(EnsurePath "${STORE_PATH}/loc")
    
    EchoSection "RUNNING CLOC"
    LoCSummaryFile "${project}" "${project}.Docs" "${project}.Tests" > "${path}/index.md"
    LoCToCFile "${project}" "${project}.Docs" "${project}.Tests" > "${path}/toc.yml"

    LoCDetailsFile "${project}" > "${path}/${project}.md"
    LoCDetailsFile "${project}.Docs" > "${path}/${project}.Docs.md"
    LoCDetailsFile "${project}.Tests" > "${path}/${project}.Tests.md"
    EchoSectionEnd
    
    EchoSection "BUILDING DOCS FILES"
    local reports_path=$(EnsurePath "${STORE_PATH}/_docs/before/reports")
    cat "${path}/index.md" > "${reports_path}/loc.md"
    EchoSectionAnd
    local loc_path=$(EnsurePath "${reports_path}/loc")
    cp -r --verbose "${path}/." "${loc_path}"
    EchoSectionEnd
}

function PackBadges
{
    echo "======================================================================"
    echo "Badge Generation DISABLED"
    echo "======================================================================"
}
