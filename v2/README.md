# Pipeline Scripts v2

This is version 2 of the pipeline. Primarily this version addresses support for multiple reports and generally better separation of temporary artifacts and publishable artifacts.
