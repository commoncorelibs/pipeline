#!/usr/bin/env bash
# Copyright (c) 2022 Jay Jeckel
# Licensed under the MIT license: https://opensource.org/licenses/MIT
# Permission is granted to use, copy, modify, and distribute the work.
# Full license information available in the project LICENSE file.

set -o errexit
set -o nounset
set -o pipefail
#set -o xtrace

function RunTest
{
    echo "======================================================================"
    echo "INSTALLING libgdiplus"
    echo "----------------------------------------------------------------------"
    apt-get update -y -qq && apt-get install -y -qq --allow-unauthenticated libc6-dev libgdiplus libx11-dev
    rm -rf /var/lib/apt/lists/*
    echo "======================================================================"

    echo "======================================================================"
    echo "INSTALLING coverlet.console"
    echo "----------------------------------------------------------------------"
    dotnet tool install coverlet.console --tool-path tools
    echo "======================================================================"

    echo "======================================================================"
    echo "RUNNING dotnet test"
    echo "----------------------------------------------------------------------"
    dotnet test "${project}.Tests/${project}.Tests.csproj" -c Release
    echo "======================================================================"

    echo "======================================================================"
    echo "ANALYZE TEST COVERAGE"
    echo "----------------------------------------------------------------------"
    tools/coverlet "${project}.Tests/bin/Release/net5.0/${project}.Tests.dll" --target "dotnet" --targetargs "test ${project}.Tests/${project}.Tests.csproj --no-build -c Release" --output "${stats_path}/" -f json -f opencover | tee "${stats_path}/coverage.txt"
    echo "======================================================================"
}
