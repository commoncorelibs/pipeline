#!/usr/bin/env bash
# Copyright (c) 2022 Jay Jeckel
# Licensed under the MIT license: https://opensource.org/licenses/MIT
# Permission is granted to use, copy, modify, and distribute the work.
# Full license information available in the project LICENSE file.

set -o errexit
set -o nounset
set -o pipefail
#set -o xtrace

function ValidateVariable
{
if [ -z "${1:-}" ]; then
    echo "ERROR: Variable '${2}' is empty or not set."
    exit 1
fi;
}

function LoadVersion { version="$(cat ./version.txt)"; version="${version:-0.0.0}"; }

function DevBumpVersion
{
local array=($(echo "${version:-0.0.0}" | tr '.-' ' '))
if [ ${#array[@]} -eq 3 ]; then
    version="${array[0]}.${array[1]}.$((${array[2]}+1))-pre.0"
else
    version="${array[0]}.${array[1]}.${array[2]}-${array[3]}.$((${array[4]}+1))"
fi;
unset array
}

newline=$'\n'

base_path="$(pwd)"
public_path="${base_path}/public"
product_path="${public_path}/product"
mkdir --parents "${product_path}"
reports_path="${public_path}/reports"
mkdir --parents "${reports_path}"
stats_path="${public_path}/stats"
mkdir --parents "${stats_path}"

echo "======================================================================"
echo "VERIFY BASH INSTALLED"
echo "----------------------------------------------------------------------"
bash --version
echo "======================================================================"

echo "======================================================================"
echo "VERIFY VARIABLES DECLARED"
echo "----------------------------------------------------------------------"
echo "Verify Variable: company"
ValidateVariable "${company:-}" "company"
echo "Final Value: ${company}"
echo "----------------------------------------------------------------------"
echo "Verify Variable: project"
ValidateVariable "${project:-}" "project"
echo "Final Value: ${project}"
echo "----------------------------------------------------------------------"
echo "Verify Variable: pipeline_path"
ValidateVariable "${pipeline_path:-}" "pipeline_path"
echo "Final Value: ${pipeline_path}"
echo "======================================================================"

echo "======================================================================"
echo "INITIALIZE VERSION VARIABLE"
echo "----------------------------------------------------------------------"
LoadVersion
echo "Initial Version: ${version}"
if [ -z "${CI_COMMIT_TAG:-}" ]; then
    echo "Tagless Commit Detected: Bumping to temp dev version."
    DevBumpVersion
fi;
echo "Final Value: ${version}"
echo "======================================================================"

echo "======================================================================"
echo "INITIALIZE TITLE VARIABLE"
echo "----------------------------------------------------------------------"
title="${company}.${project}.${version}"
echo "Final Value: ${title}"
echo "======================================================================"
