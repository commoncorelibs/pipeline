#!/usr/bin/env bash
# Copyright (c) 2022 Jay Jeckel
# Licensed under the MIT license: https://opensource.org/licenses/MIT
# Permission is granted to use, copy, modify, and distribute the work.
# Full license information available in the project LICENSE file.

set -o errexit
set -o nounset
set -o pipefail
#set -o xtrace

function PackNuget
{
    echo "======================================================================"
    echo "RUNNING dotnet pack"
    echo "----------------------------------------------------------------------"
    dotnet pack ${project}/${project}.csproj -c Release -o "${product_path}" /p:Version=${version}
    echo "======================================================================"
}

function PackZip
{
    local platform="${1}"
    local framework="${2:-}"
    local temp_path=$(mktemp -d)
    local zip_path=""

    if [ -z "${framework}" ]; then
        zip_path="${product_path}/${title}-${platform}.zip"
    else
        zip_path="${product_path}/${title}-${platform}-${framework}.zip"
    fi;

    echo "======================================================================"
    echo "GENERATING FILES"
    echo "platform:  ${platform}"
    echo "framework: ${framework:-null}"
    echo "----------------------------------------------------------------------"
    if [ -z "${framework}" ]; then
        dotnet publish ${project}/${project}.csproj -r "${platform}" --self-contained false -c Release -o "${temp_path}" /p:Version=${version}
    else
        dotnet publish ${project}/${project}.csproj -r "${platform}" --framework "${framework}" --self-contained false -c Release -o "${temp_path}" /p:Version=${version}
    fi;
    echo "======================================================================"

    if ! [ -x "$(command -v zip)" ]; then
    echo "======================================================================"
    echo "INSTALLING zip"
    echo "----------------------------------------------------------------------"
    apt-get update -y -qq && apt-get install -y -qq --allow-unauthenticated zip
    echo "======================================================================"
    fi;

    echo "======================================================================"
    echo "ZIPPING FILES"
    echo "zip_path: ${zip_path}"
    echo "----------------------------------------------------------------------"
    cd "${temp_path}"
    zip -r "${zip_path}" *
    cd "${base_path}"
    echo "======================================================================"
}

function PackDocs
{
    echo "======================================================================"
    echo "RUNNING DOCFX"
    echo "----------------------------------------------------------------------"
    docfx ${project}.Docs/docfx.json --force
    echo "======================================================================"

    echo "======================================================================"
    echo "PACK DOCS"
    echo "----------------------------------------------------------------------"
    cp -r -v "${project}.Docs/_site/." "${public_path}"
    echo "======================================================================"
}

function PackReports
{
    echo "======================================================================"
    echo "INSTALLING dotnet-reportgenerator-globaltool"
    echo "----------------------------------------------------------------------"
    dotnet tool install dotnet-reportgenerator-globaltool --tool-path tools
    echo "======================================================================"

    echo "======================================================================"
    echo "RUNNING REPORTGENERATOR"
    echo "----------------------------------------------------------------------"
    tools/reportgenerator "-reports:${stats_path}/coverage.opencover.xml" "-targetdir:${reports_path}" "-reportTypes:HTMLInline;HTMLSummary;HTMLChart;MHTML"
    echo "======================================================================"

    #echo "======================================================================"
    #echo "REPLACE HTM REFERENCES"
    #echo "----------------------------------------------------------------------"
    #sed -i 's/\.htm/\.html/g' "${reports_path}/*.htm"
    #echo "======================================================================"

    #echo "======================================================================"
    #echo "RENAME HTM FILES"
    #echo "----------------------------------------------------------------------"
    #for f in "${reports_path}/*.htm"; do mv --verbose "$f" "${f%htm}html"; done
    #echo "======================================================================"
}

function PackBadges
{
    echo "======================================================================"
    echo "INITIALIZING SSH"
    echo "----------------------------------------------------------------------"
    mkdir -p ~/.ssh && chmod 700 ~/.ssh
    ssh-keyscan gitlab.com >> ~/.ssh/known_hosts && chmod 644 ~/.ssh/known_hosts
    eval $(ssh-agent -s)
    ssh-add <(echo "${SSH_PRIVATE_KEY}")
    echo "======================================================================"

    echo "======================================================================"
    echo "INSTALLING python-gitlab"
    echo "----------------------------------------------------------------------"
    pip install python-gitlab
    echo "======================================================================"

    echo "======================================================================"
    echo "MAKING DIRECTORIES"
    echo "----------------------------------------------------------------------"
    mkdir -p --verbose public/badge/{coverage,issues,tests,version}
    echo "======================================================================"

    echo "======================================================================"
    echo "RUNNING BADGE GEN"
    echo "----------------------------------------------------------------------"
    python3 "${__dirpath}/../Tools/gen-badges.py" --token "${GitlabApiKey}" --pid "${CI_PROJECT_ID}" --coverage "${stats_path}/coverage.txt"
    echo "======================================================================"
}
