#!/usr/bin/env bash
# Copyright (c) 2022 Jay Jeckel
# Licensed under the MIT license: https://opensource.org/licenses/MIT
# Permission is granted to use, copy, modify, and distribute the work.
# Full license information available in the project LICENSE file.

set -o errexit
set -o nounset
set -o pipefail
#set -o xtrace

function DeployPages
{
    echo "======================================================================"
    echo "DEPLOYING PAGES"
    echo "${CI_PAGES_URL}"
    echo "----------------------------------------------------------------------"
    ls --file-type --sort=extension "${public_path}"
    echo "======================================================================"
}

function DeployNuget
{
    local nuget_path="${product_path}/${title}.nupkg"
    echo "======================================================================"
    echo "Running NUGET dotnet nuget push"
    echo "${nuget_path}"
    echo "----------------------------------------------------------------------"
    dotnet nuget push "${nuget_path}" -k "${NugetKey}" -s "https://api.nuget.org/v3/index.json"
    echo "======================================================================"
}

function DeployRelease
{
    local json_response=""

    echo "======================================================================"
    echo "INSTALLING curl"
    echo "----------------------------------------------------------------------"
    apt-get update -y -qq && apt-get install -y -qq --allow-unauthenticated curl
    #apk add curl
    echo "======================================================================"

    echo "======================================================================"
    echo "INSTALLING jq"
    echo "----------------------------------------------------------------------"
    apt-get update -y -qq && apt-get install -y -qq --allow-unauthenticated jq
    #apk update && apk add jq
    echo "======================================================================"
    
    echo "======================================================================"
    echo "LIST RELEASES"
    echo "----------------------------------------------------------------------"

    json_response=$(curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/releases")
    
    echo "json_response: ${json_response}"
    echo "======================================================================"

    echo "======================================================================"
    echo "TEMP DELETE RELEASE FOR TESTING PURPOSES"
    echo "----------------------------------------------------------------------"

    json_response=$(curl --request DELETE --header "JOB-TOKEN: ${CI_JOB_TOKEN}" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/releases/${CI_COMMIT_TAG}")
    
    echo "json_response: ${json_response}"
    echo "======================================================================"

    echo "======================================================================"
    echo "CREATE RELEASE"
    echo "----------------------------------------------------------------------"
    local json_request='{ "name": "'"${CI_COMMIT_TAG}"'", "tag_name": "'"${CI_COMMIT_TAG}"'", "description": "'"# Release v${CI_COMMIT_TAG}"'" }'
    echo "json_request: ${json_request}"

    json_response=$(curl --request POST --header 'Content-Type: application/json' --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --data "${json_request}" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/releases")

    echo "json_response: ${json_response}"
    echo "======================================================================"

    echo "======================================================================"
    echo "ATTACH ASSETS"
    echo "----------------------------------------------------------------------"
    for file_path in ${product_path}/*
    do
        file_name="$(basename "${file_path}")"
        echo "UPLOAD ASSET"
        echo "${file_name}"
        echo "${file_path}"
        echo "----------------------------------------------------------------------"

        json_response=$(curl --request POST --header "PRIVATE-TOKEN: ${GitlabApiKey}" --form "file=@${file_path}" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/uploads")

        echo "json_response: ${json_response}"

        local rel_url=$(jq -r '.url' <<<"${json_response}")
        local full_url="${CI_PROJECT_URL}${rel_url}"
        echo "----------------------------------------------------------------------"
        echo "LINK ASSET"
        echo "${rel_url}"
        echo "${full_url}"
        echo "----------------------------------------------------------------------"

        json_response=$(curl --request POST --header "PRIVATE-TOKEN: ${GitlabApiKey}" --data "name=${file_name}" --data "url=${full_url}" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/releases/${CI_COMMIT_TAG}/assets/links")

        echo "json_response: ${json_response}"
        echo "----------------------------------------------------------------------"
    done
    echo "======================================================================"

    exit 0

    echo "======================================================================"
    echo "UPLOAD ARTIFACTS"
    echo "----------------------------------------------------------------------"
    #for filename in "${title}-linux-x64-netstandard2.0.zip" "${title}-linux-x64-netstandard2.1.zip" "${title}-mac-x64-netstandard2.0.zip" "${title}-mac-x64-netstandard2.1.zip" "${title}-win-x64-netstandard2.0.zip" "${title}-win-x64-netstandard2.1.zip" "${title}.nupkg"
    for filepath in ls public/product/*
    do
    filename="$(basename ${filepath})"
    echo "======================================================================"
    echo "UPLOAD $filename ($filepath)"
    echo "----------------------------------------------------------------------"
    #filepath="public/product/${filename}"
    json_response=$(curl --request POST --header "PRIVATE-TOKEN: ${GitlabApiKey}" --form "file=@${filepath}" ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/uploads)
    echo "${json_response}"
    # get the url from the json return
    rel_url=$(jq -r '.url' <<<"${json_response}")
    echo "======================================================================"
    echo "LINK ASSET $filename ($filepath) ==> ${CI_PROJECT_URL}${rel_url}"
    echo "----------------------------------------------------------------------"
    curl --request POST --header "PRIVATE-TOKEN: ${GitlabApiKey}" --data "name=${filename}" --data "url=${CI_PROJECT_URL}${rel_url}" ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/releases/${CI_COMMIT_TAG}/assets/links
    done
    echo "======================================================================"
}
