import enum
import os
import re
import subprocess


class BumpType(enum.Enum):
    Major = 1
    Minor = 2
    Patch = 3
    Pre = 4
    Build = 5

class Bumper(object):
    """description of class"""
    def __init__(self, token=None):
        ''' Constructor for this class. '''
        #self.Token = token
        #self.Lab = gitlab.Gitlab('https://gitlab.com/', private_token=token) if token else None
        #self.FetchTags()

    RE_VERSION = re.compile(r'(\d+)\.(\d+)\.(\d+)(?:-(\w+))?(?:\+(\w+))?')

    def Git(self, *args):
        return subprocess.check_output(["git"] + list(args))

    def WriteFile(self, path, content):
        # Validate path (part 1)
        if path.endswith('/'):
            raise Exception('File location may not be a directory.')

        # Get absolute filepath
        path = os.path.abspath(path)
        if not path.lower().endswith('.svg'):
            path += '.svg'

        with open(path, mode='w') as handle:
            handle.write(content)

    #def FetchTags(self):
    #    self.Git("fetch", "--tags")

    def Sorter_Semver(self, version):
        major, minor, patch, pre, build = self.RE_VERSION.search(version).groups()
        return [int(v) for v in (major, minor, patch)] + [not pre, pre or 0, build or 0]

    def SortSemver(self, versions, ascending=True):
        return sorted(versions, key=self.Sorter_Semver, reverse=not ascending)

    #def GetTags(self, ascending=True):
    #    return self.SortSemver(self.Git("tag", "--list").decode().strip().splitlines(), ascending)

    def GetTagLatest(self, versions, stable=False):
        if len(versions) > 0:
            if not stable:
                return versions[0]
            for version in versions:
                res = self.RE_VERSION.search(version)
                major, minor, patch, pre, build = res.groups()
                if not pre:
                    return version
        return "0.0.0"

    #def GetLatestCommitTag(self):
    #    try:
    #        latest = self.Git("describe").decode().strip()
    #    except subprocess.CalledProcessError:
    #        return None
    #    else:
    #        return latest if '-' in latest else None

    #def GetCommitMessage(self, version):
    #    return self.Git("show", "-s", "--format=%B", version).decode().strip()

    def TagRepo(self, version):
        url = os.environ["CI_REPOSITORY_URL"]

        # Transforms the repository URL to the SSH URL
        # Example input: https://gitlab-ci-token:xxxxxxxxxxxxxxxxxxxx@gitlab.com/threedotslabs/ci-examples.git
        # Example output: git@gitlab.com:threedotslabs/ci-examples.git
        push_url = re.sub(r'.+@([^/]+)/', r'git@\1:', url)

        self.Git("remote", "set-url", "--push", "origin", push_url)
        self.Git("tag", version)
        self.Git("push", "origin", version)

    def GetMessageBumpType(self, message):
        if not message:
            return None
        if any(s in message for s in ("[major]", "[breaking]")):
            return BumpType.Major
        elif any(s in message for s in ("[minor]", "[feature]")):
            return BumpType.Minor
        elif any(s in message for s in ("[patch]", "[fix]")):
            return BumpType.Patch
        elif any(s in message for s in ("[pre]", "[bump]")):
            return BumpType.Pre
        elif any(s in message for s in ("[build]", "[work]")):
            return BumpType.Build
        else:
            return None

    def BumpVersion(self, version, bumpType):
        from .semver import bump_major, bump_minor, bump_patch, bump_prerelease, bump_build
        if bumpType == None:
            return version
        elif bumpType == BumpType.Build:
            return bump_build(version, "build.0")
        elif bumpType == BumpType.Pre:
            return bump_prerelease(version, "pre.0")
        elif bumpType == BumpType.Patch:
            return bump_patch(version)
        elif bumpType == BumpType.Minor:
            return bump_minor(version)
        elif bumpType == BumpType.Major:
            return bump_major(version)
        else:
            return version

    def Test(self, version):
        from .semver import parse
        parts = parse(version)
        print(parts["major"])
        print(parts["minor"])
        print(parts["patch"])
        print(parts["prerelease"])
        print(parts["build"])

        print()
        print("None:  " + self.BumpVersion(version, None))
        print(BumpType.Build.name + ": " + self.BumpVersion(version, BumpType.Build))
        print(BumpType.Pre.name + ":   " + self.BumpVersion(version, BumpType.Pre))
        print(BumpType.Patch.name + ": " + self.BumpVersion(version, BumpType.Patch))
        print(BumpType.Minor.name + ": " + self.BumpVersion(version, BumpType.Minor))
        print(BumpType.Major.name + ": " + self.BumpVersion(version, BumpType.Major))
