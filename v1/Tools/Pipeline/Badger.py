

class Badger(object):
    """description of class"""
    def __init__(self):
        ''' Constructor for this class. '''

    # Ideas:
    # Activity: https://shields.io/category/activity

    COLORS = {
        "green": "#33CC11",#"#00FF00",#
        "blue": "#0088CC",#"#0000FF",#
        "yellow": "#DDBB11",#"#DFB317",#
        "orange": "#FF7733",#"#E0321F",#
        "red": "#EE4433",#"#FF0000",#
        "lightgrey": "#999999",#"#9F9F9F",#
    }

    def GenBadge(self, title, content, color):
        from .anybadge import Badge
        badge = Badge(title, content, default_color=color)
        return badge.badge_svg_text

    def GenVersionBadge(self, title, version):
        from .semver import parse_version_info
        info = parse_version_info(version)
        color = self.COLORS["blue"] if not info.prerelease else self.COLORS["yellow"]
        return self.GenBadge(title, "v" + version, color)

    def GenTestsBadge(self, title, passCount, failCount, skipCount):
        usePassed = passCount is not None and passCount >= 0
        useFailed = failCount is not None and failCount >= 0
        useSkipped = skipCount is not None and skipCount >= 0
        if usePassed and useFailed and useSkipped:
            color = self.COLORS["red"] if failCount > 0 else self.COLORS["blue"] if skipCount > 0 else self.COLORS["green"]
            return self.GenBadge(title, f"✔ {passCount} | ✘ {failCount} | ➟ {skipCount}", color)

        elif usePassed and useFailed:
            color = self.COLORS["red"] if failCount > 0 else self.COLORS["green"]
            return self.GenBadge(title, f"✔ {passCount} | ✘ {failCount}", color)

        elif usePassed and useSkipped:
            color = self.COLORS["blue"] if skipCount > 0 else self.COLORS["green"]
            return self.GenBadge(title, f"✔ {passCount} | ➟ {skipCount}", color)

        elif useFailed and useSkipped:
            color = self.COLORS["red"] if failCount > 0 else self.COLORS["blue"]
            return self.GenBadge(title, f"✘ {failCount} | ➟ {skipCount}", color)

        elif usePassed:
            color = self.COLORS["green"] if passCount > 0 else self.COLORS["blue"]
            return self.GenBadge(title, f"✔ {passCount}", color)

        elif useFailed:
            color = self.COLORS["red"] if failCount > 0 else self.COLORS["green"]
            return self.GenBadge(title, f"✘ {failCount}", color)

        elif useSkipped:
            color = self.COLORS["blue"] if skipCount > 0 else self.COLORS["green"]
            return self.GenBadge(title, f"➟ {skipCount}", color)

        else:
            return self.GenBadge(title, "unknown", self.COLORS["lightgrey"])

    def GenCoverageBadge(self, title, coverageTotal, coverageAverage):
        useTotal = coverageTotal is not None and coverageTotal >= 0
        useAverage = coverageAverage is not None and coverageAverage >= 0
        thresholds={25: "#EE4433", 50: "#FF7733", 75: "#DDBB11", 101: "#33CC11"}
        color = self.COLORS["lightgrey"]
        for n in thresholds:
            if float(coverageTotal if useTotal else coverageAverage if useAverage else 0) < float(n):
                color = thresholds[n]
                break
        if useTotal and useAverage:
            return self.GenBadge(title, f"{coverageTotal}% | X̄ {coverageAverage}%", color)
        
        elif useTotal:
            return self.GenBadge(title, f"{coverageTotal}%", color)
        
        elif useAverage:
            return self.GenBadge(title, f"X̄ {coverageAverage}%", color)

        else:
            return self.GenBadge(title, "unknown", self.COLORS["lightgrey"])

    def GenIssuesBadge(self, title, openCount, closeCount):
        useOpen = openCount is not None and openCount >= 0
        useClose = closeCount is not None and closeCount >= 0
        color = self.COLORS["blue"]
        if useOpen and useClose:
            return self.GenBadge(title, f"🞇 {openCount} | ✔ {closeCount}", color)
        
        elif useOpen:
            return self.GenBadge(title, f"🞇 {openCount}", color)
        
        elif useClose:
            return self.GenBadge(title, f"✔ {closeCount}", color)

        else:
            return self.GenBadge(title, "unknown", self.COLORS["lightgrey"])

    def CreateBadge(self, key, value, *args):
        result = None
        if key == "version":
            if value is None or value == "default" or value == "stable":
                result = self.GenVersionBadge("stable", args[0])
            elif value == "latest":
                result = self.GenVersionBadge("latest", args[0])
            elif value == "dev":
                result = self.GenVersionBadge("dev", args[0])
            else:
                result = self.GenBadge("version", value if value else "unknown", self.COLORS["lightgrey"])

        elif key == "tests":
            if value is None or value == "default" or value == "passfail":
                result = self.GenTestsBadge("tests", int(args[0]), int(args[1]), None)
            elif value == "all":
                result = self.GenTestsBadge("tests", int(args[0]), int(args[1]), int(args[2]))
            elif value == "passed":
                result = self.GenTestsBadge("tests", int(args[0]), None, None)
            elif value == "failed":
                result = self.GenTestsBadge("tests", None, int(args[0]), None)
            elif value == "skipped":
                result = self.GenTestsBadge("tests", None, None, int(args[0]))
            else:
                result = self.GenBadge("tests", value if value else "unknown", self.COLORS["lightgrey"])

        elif key == "coverage":
            if value is None or value == "default":
                result = self.GenCoverageBadge("coverage", int(args[0]), int(args[1]))
            elif value == "total":
                result = self.GenCoverageBadge("coverage", int(args[0]), None)
            elif value == "average":
                result = self.GenCoverageBadge("coverage", None, int(args[0]))
            else:
                result = self.GenBadge("coverage", value if value else "unknown", self.COLORS["lightgrey"])

        elif key == "issues":
            if value is None or value == "default":
                result = self.GenIssuesBadge("issues", int(args[0]), int(args[1]))
            elif value == "opened":
                result = self.GenIssuesBadge("issues", int(args[0]), None)
            elif value == "closed":
                result = self.GenIssuesBadge("issues", None, int(args[0]))
            else:
                result = self.GenBadge("issues", value if value else "unknown", self.COLORS["lightgrey"])

        else:
            result = self.GenBadge(key if key else "none", value if value else "unknown", self.COLORS["lightgrey"])
        return result
