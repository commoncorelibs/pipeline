#!/usr/bin/env python3
import os
import re


def output_badge(title, path, badge, write=True, verbose=False):
    if verbose:
        print()
        print(f"Writing '{title}' badge...")
        print(path)
        if not write:
            print(badge)
    else:
        print(f"Writing '{title}' badge: {path}")
    if (write):
        write_file(path, badge)
    if verbose:
        print("Done writing {title}.")

def write_file(path, content):
    # Validate path (part 1)
    if path.endswith('/'):
        raise Exception('File location may not be a directory.')

    # Get absolute filepath
    path = os.path.abspath(path)
    if not path.lower().endswith('.svg'):
        path += '.svg'

    with open(path, mode='w', encoding='utf-8') as handle:
        handle.write(content)

def parse_args():
    """Parse the command line arguments."""
    import argparse
    import textwrap
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description=textwrap.dedent('''Some description.'''))
    parser.add_argument("-t", "--token", type=str, help="The Gitlab private token used for API calls.")
    parser.add_argument("-i", "--pid", type=int, help="The Gitlab project id used for API calls.")
    parser.add_argument("-c", "--coverage", type=str, help="Path to the coverage file.")
    return parser.parse_args()

def main():
    import gitlab
    from Pipeline import BumpType
    from Pipeline import Bumper
    from Pipeline import Badger

    print("Generating Badges...")
    args = parse_args()

    token = args.token
    if token is not None:
        print("SUCCESS: Gitlab token provided.")
    else:
        print("ERROR: Gitlab token must be provided as argument -t <token>.")
        return 1

    pid = args.pid
    if pid is not None:
        print(f"SUCCESS: Gitlab project ID '{pid}' provided.")
    else:
        print("ERROR: Gitlab project ID must be provided as argument -i <pid>.")
        return 1

    coveragePath = args.coverage
    useCoverageReport = False
    if coveragePath is None:
        print("WARNING: Path to coverage report not provided.")
        print("WARNING: Generation of test and coverage badges will read as unknown.")
    elif not os.path.exists(coveragePath):
        print("WARNING: Path to provided coverage report does not exist.")
        print(coveragePath)
        print("WARNING: Generation of test and coverage badges will read as unknown.")
    elif not os.path.isfile(coveragePath):
        print("WARNING: Path to provided coverage report is not a file.")
        print(coveragePath)
        print("WARNING: Generation of test and coverage badges will read as unknown.")
    else:
        useCoverageReport = True
        print("SUCCESS: Path to coverage report provided.")
        print(coveragePath)
        
    print("Initializing the badger...")
    bumper = Bumper()
    badger = Badger()
    writing = True
    print("Writing Files: " + ("Enabled" if writing else "Disabled"))
    verbose = False
    print("Verbose Output: " + ("Enabled" if verbose else "Disabled"))
    if verbose:
        print("Done!")
        
    if verbose:
        print("Initializing Gitlab API and locating project...")
    lab = gitlab.Gitlab('https://gitlab.com/', private_token=token)
    project = lab.projects.get(pid)
    if verbose:
        print("SUCCESS: Done!")
    else:
        print("SUCCESS: Initializing Gitlab API and locating project.")

    # TODO: Port directory creation from the ci yml.
    #os.makedirs("path/to/directory", exist_ok=True)
    
    if verbose:
        print()
        print("Generating version badges...")
    versions = [o.name for o in project.tags.list(all=True)]
    versions = bumper.SortSemver(versions, False)
    if versions is None or len(versions) == 0:
        versionStable = "0.0.0"
        versionLatest = "0.0.0"
        versionDev = "0.0.0"
    else:
        versionStable = bumper.GetTagLatest(versions, True)
        versionLatest = bumper.GetTagLatest(versions, False)
        versionDev = bumper.BumpVersion(bumper.GetTagLatest(versions, False), BumpType.Pre)
    print("Version Stable: " + versionStable)
    print("Version Latest: " + versionLatest)
    print("Version Dev:    " + versionDev)

    defaultBadge = None
    
    defaultBadge = badge = badger.GenVersionBadge("stable", versionStable)
    output_badge("version stable", "public/badge/version/stable.svg", badge, writing, verbose)

    badge = badger.GenVersionBadge("latest", versionLatest)
    output_badge("version latest", "public/badge/version/latest.svg", badge, writing, verbose)

    badge = badger.GenVersionBadge("dev", versionDev)
    output_badge("version dev", "public/badge/version/dev.svg", badge, writing, verbose)

    output_badge("version", "public/badge/version.svg", defaultBadge, writing, verbose)
    output_badge("version default", "public/badge/version/default.svg", defaultBadge, writing, verbose)

    if verbose:
        print("SUCCESS: Done!")
    
    if verbose:
        print()
        print("Generating issues badges...")
    issueOpened = len(project.issues.list(all=True, state='opened'))
    issueClosed = len(project.issues.list(all=True, state='closed'))
    print("Issues Opened: " + str(issueOpened))
    print("Issues Closed: " + str(issueClosed))

    defaultBadge = None

    badge = badger.GenIssuesBadge("issues", issueOpened, None)
    output_badge("issues opened", "public/badge/issues/opened.svg", badge, writing, verbose)

    badge = badger.GenIssuesBadge("issues", None, issueClosed)
    output_badge("issues closed", "public/badge/issues/closed.svg", badge, writing, verbose)
    
    defaultBadge = badge = badger.GenIssuesBadge("issues", issueOpened, issueClosed)
    output_badge("issues openclosed", "public/badge/issues/openclosed.svg", badge, writing, verbose)

    output_badge("issues", "public/badge/issues.svg", defaultBadge, writing, verbose)
    output_badge("issues default", "public/badge/issues/default.svg", defaultBadge, writing, verbose)

    if verbose:
        print("SUCCESS: Done!")

    #coveragePath = os.path.join(script_root, "coverage.txt")
    #print(coveragePath)
    if useCoverageReport:
        with open(coveragePath, 'r', encoding='utf-8') as file:
            text = file.read()

        if verbose:
            print()
            print("Generating tests badges...")
        matchTotal = re.search(r"Total(?:tests)?:.*?(\d+)", text)
        testTotal = int(0 if matchTotal is None else matchTotal.group(1))
        matchPassed = re.search(r"Passed:.*?(\d+)", text)
        testPassed = int(0 if matchPassed is None else matchPassed.group(1))
        testFailed = testTotal - testPassed
        testSkipped = 0
        print("Tests Total:   " + str(testTotal))
        print("Tests Passed:  " + str(testPassed))
        print("Tests Failed:  " + str(testFailed))
        print("Tests Skipped: " + str(testSkipped))

        defaultBadge = None

        badge = badger.GenTestsBadge("tests", testPassed, None, None)
        output_badge("tests passed", "public/badge/tests/passed.svg", badge, writing, verbose)

        badge = badger.GenTestsBadge("tests", None, testFailed, None)
        output_badge("tests failed", "public/badge/tests/failed.svg", badge, writing, verbose)

        badge = badger.GenTestsBadge("tests", None, None, testSkipped)
        output_badge("tests skipped", "public/badge/tests/skipped.svg", badge, writing, verbose)

        defaultBadge = badge = badger.GenTestsBadge("tests", testPassed, testFailed, None)
        output_badge("tests passfail", "public/badge/tests/passfail.svg", badge, writing, verbose)

        badge = badger.GenTestsBadge("tests", testPassed, testFailed, testSkipped)
        output_badge("tests passfailskip", "public/badge/tests/passfailskip.svg", badge, writing, verbose)
    
        output_badge("tests", "public/badge/tests.svg", defaultBadge, writing, verbose)
        output_badge("tests default", "public/badge/tests/default.svg", defaultBadge, writing, verbose)

        if verbose:
            print("SUCCESS: Done!")
       
        if verbose:
            print()
            print("Generating coverage badges...")
        matchTotal = re.search(r"Total.*?(\d+(?:\.\d+)?)%", text)
        coverageTotal = float(0 if matchTotal is None else 0 if matchTotal is "NaN" else matchTotal.group(1))
        matchAverage = re.search(r"Average.*?(\d+(?:\.\d+)?)%", text)
        coverageAverage = float(0 if matchAverage is None else 0 if matchAverage is "NaN" else matchAverage.group(1))
        print("Coverage Total:   " + str(coverageTotal) + "%")
        print("Coverage Average: " + str(coverageAverage) + "%")

        defaultBadge = None

        defaultBadge = badge = badger.GenCoverageBadge("coverage", coverageTotal, None)
        output_badge("coverage total", "public/badge/coverage/total.svg", badge, writing, verbose)

        badge = badger.GenCoverageBadge("coverage", None, coverageAverage)
        output_badge("coverage average", "public/badge/coverage/average.svg", badge, writing, verbose)
        
        badge = badger.GenCoverageBadge("coverage", coverageTotal, coverageAverage)
        output_badge("coverage totalaverage", "public/badge/coverage/totalaverage.svg", badge, writing, verbose)

        output_badge("coverage", "public/badge/coverage.svg", defaultBadge, writing, verbose)
        output_badge("coverage default", "public/badge/coverage/default.svg", defaultBadge, writing, verbose)

        if verbose:
            print("SUCCESS: Done!")
    else:   
        if verbose:
            print()
            print("Generating tests badges with value as 'unknown'...")
        badge = badger.GenBadge("tests", "unknown", badger.COLORS["lightgrey"])
        output_badge("tests", "public/badge/tests.svg", badge, writing, verbose)
        output_badge("tests default", "public/badge/tests/default.svg", badge, writing, verbose)
        output_badge("tests passfail", "public/badge/tests/passfail.svg", badge, writing, verbose)
        output_badge("tests passed", "public/badge/tests/passed.svg", badge, writing, verbose)
        output_badge("tests failed", "public/badge/tests/failed.svg", badge, writing, verbose)
        output_badge("tests skipped", "public/badge/tests/skipped.svg", badge, writing, verbose)
        output_badge("tests passfailskip", "public/badge/tests/passfailskip.svg", badge, writing, verbose)
        if verbose:
            print("SUCCESS: Done!")
           
        if verbose:
            print()
            print("Generating coverage badges with value as 'unknown'...")
        badge = badger.GenBadge("coverage", "unknown", badger.COLORS["lightgrey"])
        output_badge("coverage", "public/badge/coverage.svg", badge, writing, verbose)
        output_badge("coverage default", "public/badge/coverage/default.svg", badge, writing, verbose)
        output_badge("coverage total", "public/badge/coverage/total.svg", badge, writing, verbose)
        output_badge("coverage average", "public/badge/coverage/average.svg", badge, writing, verbose)
        output_badge("coverage totalaverage", "public/badge/coverage/totalaverage.svg", badge, writing, verbose)
        if verbose:
            print("SUCCESS: Done!")

    print("!! ALERT !! ALERT !! ALERT !!")
    print("When run under CI/CD, this script only creates the badges as pipeline artifacts.")
    print("The Pages deploy job must now be run before the artifacts expire to publish the badges.")
    print("!! ALERT !! ALERT !! ALERT !!")

    return 0


if __name__ == "__main__":
    import sys
    try:
        sys.exit(main())
    except SystemExit:
        pass
    else:
        pass
