# Pipeline Includes

Provides include yaml files with templates for common attributes such as `image`, `only`, and `when`.

The files can be included in a gitlab ci yaml file through the `include` section.

```yaml
include:
    - project: "commoncorelibs/pipeline"
      file: ["/v1/Includes/templates.yml"]
```

The templates are then referenced through the `extends` attribute. In this case, the `test` job will use dotnet 5 as its `image` attribute, the `only` attribute will allow **both** tagged and untagged commits, and `when` attribute will cause the job to run on success.

```yaml
test:
    extends: [.image_dotnet_5, .only_both, .when_success]
```

The above use of extends is roughly equivalent to the following job.

```yaml
test:
    image: mcr.microsoft.com/dotnet/sdk:5.0
    only: ['master', 'tags']
    when: on_success
```
